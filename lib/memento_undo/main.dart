import '../memento_multiple/generic_caretaker.dart';
import '../memento_multiple/post_model.dart';
import 'package:flutter/material.dart';

class Main extends StatefulWidget {
  @override
  _MainState createState() => _MainState();
}

class _MainState extends State<Main> {
  TextEditingController _textEditingController;
  GenericCareTaker<Post> postCareTaker;
  Post post;
  int currentIndex = 0;

  @override
  void initState() {
    super.initState();
    _textEditingController = new TextEditingController();
    post = new Post(_textEditingController.text);
    postCareTaker = new GenericCareTaker<Post>();
    _textEditingController.addListener(() {
      post.name = _textEditingController.text;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          TextField(
            controller: _textEditingController,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              buildGetBefore(),
              buildSave(),
              buildGetNext(),
            ],
          ),
        ],
      ),
    );
  }

  InkWell buildGetNext() {
    return InkWell(
      child: Padding(
        padding: EdgeInsets.all(10),
        child: Icon(Icons.rotate_right),
      ),
      onTap: () {
        if (postCareTaker.mementoList.isEmpty) return;
        if (postCareTaker.mementoList.length - 1 > this.currentIndex) {
          this.currentIndex++;
        }
        post.restore(postCareTaker.mementoList[this.currentIndex]);
        _textEditingController.text = post.name;
      },
    );
  }

  InkWell buildSave() {
    return InkWell(
      child: Padding(
        padding: EdgeInsets.all(10),
        child: Text("Add"),
      ),
      onTap: () {
        postCareTaker.mementoList.add(post.save());
        this.currentIndex++;
        _textEditingController.text = "";
      },
    );
  }

  InkWell buildGetBefore() {
    return InkWell(
      child: Padding(
        padding: EdgeInsets.all(10),
        child: Icon(Icons.rotate_left),
      ),
      onTap: () {
        if (postCareTaker.mementoList.isEmpty) return;
        if (this.currentIndex != 0) {
          this.currentIndex--;
        }
        post.restore(postCareTaker.mementoList[this.currentIndex]);
        _textEditingController.text = post.name;
      },
    );
  }
}
