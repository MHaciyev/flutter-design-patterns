import 'package:flutter/material.dart';

class Screen extends StatelessWidget {
  final String appBarText;
  final Widget widget;

  Screen({this.widget, this.appBarText});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(appBarText),
      ),
      body: widget,
    );
  }
}
