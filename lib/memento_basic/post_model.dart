import '../memento_basic/generic_memento.dart';

class Post {
  String name;

  Post(this.name);

  GenericMemento<Post> save() {
    return GenericMemento(new Post(this.name));
  }

  void restore(GenericMemento<Post> memento) {
    this.name = memento.state.name;
  }
}
