import '../memento_basic/post_model.dart';

class GenericMemento<T> {
  T state;

  GenericMemento(this.state);
}
