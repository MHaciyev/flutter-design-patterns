import '../memento_multiple/generic_caretaker.dart';
import '../memento_multiple/post_model.dart';
import 'package:flutter/material.dart';

class Main extends StatefulWidget {
  @override
  _MainState createState() => _MainState();
}

class _MainState extends State<Main> {
  TextEditingController _textEditingController;
  GenericCareTaker<Post> postCareTaker;
  Post post;

  @override
  void initState() {
    super.initState();
    _textEditingController = new TextEditingController();
    post = new Post(_textEditingController.text);
    postCareTaker = new GenericCareTaker<Post>();
    _textEditingController.addListener(() {
      post.name = _textEditingController.text;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          TextField(
            controller: _textEditingController,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              buildSave(),
              buildRestore(),
            ],
          ),
        ],
      ),
    );
  }

  InkWell buildSave() {
    return InkWell(
      child: Padding(
        padding: EdgeInsets.all(10),
        child: Text("Save"),
      ),
      onTap: () {
        postCareTaker.mementoList.add(post.save());
      },
    );
  }

  InkWell buildRestore() {
    return InkWell(
      child: Padding(
        padding: EdgeInsets.all(10),
        child: Text("Restore"),
      ),
      onTap: () {
        if (postCareTaker.mementoList.isEmpty) return;
        post.restore(postCareTaker.mementoList.removeLast());
        _textEditingController.text = post.name;
      },
    );
  }
}
